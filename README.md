### Example voting app

Example voting app to be deployed in a Kubernetes cluster, for training purpose.

Everything is done following the instructions in the Udemy course 
[Kubernetes for Absolute Beginners](https://www.udemy.com/course/learn-kubernetes/)
from Mumshad Mannambeth.

Example Voting App itself can be found in [dockersamples](https://hub.docker.com/u/dockersamples)
Docker Hub repos.

`master` branch of this repository show simple Kubernetes app configuration to
be deployed in plain pods. To see more advanced configuration to deploy the app
and create autoscaling services behind load balancers, checkout the branch `v2`.
